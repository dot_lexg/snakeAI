SnakeAI
-------

There is a .jar available at: https://github.com/win93/snakeAI/releases/download/nw44349-submission/snakeAI.jar.
All you should is a 1.8 or higher JRE to run it. The source repository is set up for Intellij IDEA, but it should be easy enough to import into another IDE of your choosing.

A write-up for this project is also available at: https://docs.google.com/document/d/1NGkNYjRt_Y-_bUJt23d1rA4w3lptKTgs59XHHob-XwY
