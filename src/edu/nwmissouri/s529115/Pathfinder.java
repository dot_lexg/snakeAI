package edu.nwmissouri.s529115;

import processing.core.PApplet;

import java.awt.Point;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static edu.nwmissouri.s529115.SnakeWindow.Cfg.*;

public class Pathfinder {
	private final Board board;
	private Path moves;

	public Pathfinder(Board board) {
		this.board = board;
	}

	public void findPath() {
		long begin = System.currentTimeMillis();
		moves = spaceFill(board, board.getFood(), (checkBoard) ->
				spaceFill(checkBoard, checkBoard.getTail(), (x) -> true) != null);
		System.out.printf("step: %d snakeSize: %d (%dms)\n", board.getStep(), board.getSnakeSize(), System.currentTimeMillis() - begin);
	}


	public boolean hasSteps() {
		return moves != null && !moves.isEmpty();
	}

	public Direction step() {
		return moves.step();
	}

	public void paint(PApplet applet) {
		if (moves != null)
			moves.paint(applet, COLOR_PATH);
	}

	//Adapted from pseudo code https://en.wikipedia.org/wiki/A*_search_algorithm
	private static Path bestPath(Board board, Point destination) {
		int width = board.getWidth();
		int height = board.getHeight();
		Point head = board.getHead();

		CellInfo[] cells = new CellInfo[board.cells.length];
		PriorityQueue<CellInfo> openSet = new PriorityQueue<>();

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++)
				cells[y * width + x] = new CellInfo(x, y);
		}
		CellInfo headInfo = cells[head.y * width + head.x];

		openSet.add(headInfo);
		headInfo.partialPathCost = 0;
		headInfo.totalCostGuess = manhattanDistance(headInfo, destination);

		while (!openSet.isEmpty()) {
			CellInfo curCellInfo = openSet.remove();

			if (curCellInfo.equals(destination))
				return traceOf(head, curCellInfo);

			curCellInfo.closed = true;

			for (Direction dir : Direction.ALL) {
				Point neighbor = new Point(curCellInfo.x, curCellInfo.y);
				dir.applyTo(neighbor);
				if (!board.inBounds(neighbor))
					continue;

				CellInfo neighborInfo = cells[neighbor.y * width + neighbor.x];

				if (neighborInfo.closed)
					continue;
				if (!neighborInfo.canStepFuture(board, curCellInfo))
					continue;

				if (curCellInfo.partialPathCost + 1 < neighborInfo.partialPathCost) {
					neighborInfo.prev = curCellInfo;
					neighborInfo.prevDir = dir;
					neighborInfo.partialPathCost = curCellInfo.partialPathCost + 1;
					neighborInfo.totalCostGuess = curCellInfo.partialPathCost + 1 + manhattanDistance(neighborInfo, destination);
				}

				if (!openSet.contains(neighborInfo))
					openSet.add(neighborInfo);
			}
		}

		return null;
	}

	private static Path traceOf(Point origin, CellInfo cellInfo) {
		Path path = new Path(origin);
		for (CellInfo i = cellInfo; i.prev != null; i = i.prev)
			path.add(Direction.fromTo(i.prev, i));

		return path;
	}

	private static Path spaceFill(Board board, Point destination, Predicate<Board> resultCheckPredicate) {
		Path astarPath = bestPath(board, destination);
		//If we found an A* path, use it as long as the predicate matches
		if (astarPath != null) {
			Board checkBoard = new Board(board);
			for (int i = astarPath.size() - 1; i >= 0; i--)
				checkBoard.step(astarPath.get(i));
			if (resultCheckPredicate.test(checkBoard))
				return astarPath;
		}
		for (Direction dir : Direction.ALL) {
			Point p = board.getHead();
			dir.applyTo(p);
			if (board.inBounds(p) && !board.snakeSegmentAt(p)) {
				Board tempBoard = new Board(board);
				tempBoard.step(dir);
				Path spaceFillPath = spaceFill(tempBoard, destination, resultCheckPredicate);
				if (spaceFillPath != null) {
					spaceFillPath.add(dir);
					spaceFillPath.origin = board.getHead();
					return spaceFillPath;
				}
			}
		}
		return null;
	}

	public static int manhattanDistance(Point l, Point r) {
		return Math.abs(l.x - r.x) + Math.abs(l.y - r.y);
	}

	private static class CellInfo extends Point implements Comparable<CellInfo> {
		public CellInfo prev;
		public Direction prevDir;
		public int partialPathCost = Integer.MAX_VALUE;
		public int totalCostGuess = Integer.MAX_VALUE;
		public boolean closed;

		public CellInfo(int x, int y) {
			super(x, y);
		}

		/** Inconsistent with equals */
		@Override
		public int compareTo(CellInfo o) {
			int result = this.totalCostGuess - o.totalCostGuess;
			if (result == 0) {
				//Break ties by preferring deeper nodes over shallower nodes
				result = o.partialPathCost - this.partialPathCost;
			}

			if (result == 0) {
				//Break ties by preferring staying in the same direction
				if (prevDir == prev.prevDir)
					result = -1; //`this` continues in a straight line
				else if (o.prevDir == o.prev.prevDir)
					result = 1; //`o` continues in a straight line
			}
			return result;
		}


		public boolean canStepFuture(Board board, CellInfo proposedPrev) {
			if (board.snakeSegmentAtFuture(this, proposedPrev.partialPathCost - 1))
				return false;
			for (CellInfo i = proposedPrev; i != null; i = i.prev) {
				if (i.equals(this))
					return false;
			}
			return true;
		}

		@Override
		public String toString() {
			String gString = (partialPathCost == Integer.MAX_VALUE) ? "inf" : String.valueOf(partialPathCost);
			String fString = (totalCostGuess == Integer.MAX_VALUE) ? "inf" : String.valueOf(totalCostGuess);
			String str = String.format("(%d, %d) [%s %s]", x, y, gString, fString);
			if (prev != null)
				return prev + " -(" + prevDir + ")> " + str;
			return str;
		}
	}
}
