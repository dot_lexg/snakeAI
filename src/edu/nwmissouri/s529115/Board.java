package edu.nwmissouri.s529115;

import processing.core.PApplet;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

import static edu.nwmissouri.s529115.SnakeWindow.Cfg.*;

public class Board {
	/**
	 * <h3>Board data structure:</h3>
	 * The `cell` array is addressable as a 2D array via [y*width+x].
	 * <p>
	 * Each cell contains a number that determines whether a snake segment is at a given position. If the cell's value
	 * is in the range of (step - snakeSize, step], then the cell has a snake body-part. The snake's tail is where
	 * `cell[...] == step - snakeSize + 1` and the head is where `cell[...] == step`. To carry out a step in the game,
	 * `step` is incremented and the new position for the snake's head is set to `step`, and value of `head` is updated
	 * accordingly. Increasing `step` causes the tail of the snake to disappear without having to update any cell
	 * values. The effect of this setup is that any given cell value will contain the most recent `step` value for when
	 * that cell was the head of the snake. Additionally, collision checking is O(1).
	 */
	public final int[] cells;
	private final int width, height;

	private int step;
	private int snakeSize;
	private Point food;
	private Deque<Point> snakeBody;

	/**
	 * Constructs a new snake board. spawnSnake() must be called before most other methods work properly.
	 *
	 * @param width  width of the board, in cells
	 * @param height height of the board, in cells
	 */
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		this.cells = new int[width * height];
		clear();
	}

	public Board(Board b) {
		this.cells = Arrays.copyOf(b.cells, b.cells.length);
		this.width = b.width;
		this.height = b.height;
		this.step = b.step;
		this.snakeSize = b.snakeSize;
		this.food = b.getFood();
		this.snakeBody = b.snakeBody == null ? null : new ArrayDeque<>(b.snakeBody);
	}

	/** Returns the board to the newly constructed state. */
	public void clear() {
		step = 0;
		snakeSize = 0;
		food = null;
		snakeBody = null;
		for (int i = 0; i < cells.length; i++)
			cells[i] = Integer.MIN_VALUE;
	}

	/**
	 * Places a new snake on a cleared board. The snake faces rightward.
	 *
	 * @param x         tail X position of the new snake
	 * @param y         tail Y position of the new snake
	 * @param snakeSize the number of segments the snake has
	 */
	public void spawnSnake(int x, int y, int snakeSize) {
		if (snakeBody != null)
			throw new IllegalStateException("Board is not cleared.");
		snakeBody = new ArrayDeque<>(width * height);
		snakeBody.addFirst(new Point(x, y));
		this.snakeSize = 1;
		for (int i = 1; i < snakeSize; i++)
			growStep(Direction.RIGHT);
	}

	/**
	 * Move or place down food for the snake.
	 *
	 * @param x X coordinate of food placement
	 * @param y Y coordinate of food placement
	 * @return true if the food's placement is not on top of the snake
	 */
	public boolean spawnFood(int x, int y) {
		if (snakeSegmentAt(y * width + x))
			return false;
		food = new Point(x, y);
		return true;
	}

	Point proposedTail = new Point();

	/**
	 * Carries out a step in the given direction.
	 */
	public void step(Direction direction) {
		step0(direction);
		snakeBody.removeLast();
	}
	public void growStep(Direction direction) {
		step0(direction);
		snakeSize++;
	}

	private void step0(Direction direction) {
		Point proposedHead = new Point(getHead());
		direction.applyTo(proposedHead);
		if(!inBounds(proposedHead) || snakeSegmentAt(proposedHead))
			throw new AssertionError("Chosen direction " + direction + " invalid.");
		if (proposedHead.equals(food))
			food = null;
		cells[proposedHead.y * width + proposedHead.x] = ++step;
		snakeBody.addFirst(proposedHead);
	}

	public boolean inBounds(Point p) {
		return 0 <= p.x && p.x < width && 0 <= p.y && p.y < height;
	}

	public boolean inBounds(int x, int y) {
		return 0 <= x && x < width && 0 <= y && y < height;
	}

	public boolean snakeSegmentAt(Point p) {
		return snakeSegmentAt(p.y * width + p.x);
	}

	public boolean snakeSegmentAt(int x, int y) {
		return snakeSegmentAt(y * width + x);
	}

	/**
	 * @param index usually takes the form of {@code loc(pointObject)}
	 * @return true if the specified cell value means that the cell contains a snake segment.
	 */
	private boolean snakeSegmentAt(int index) {
		return step  - snakeSize < cells[index] && cells[index] <= step;
	}

	public boolean snakeSegmentAtFuture(Point p, int stepOffset) {
		return snakeSegmentAtFuture(p.y * width + p.x, stepOffset);
	}

	private boolean snakeSegmentAtFuture(int index, int stepOffset) {
		return step + stepOffset - snakeSize < cells[index] && cells[index] <= step + stepOffset;
	}

	/**
	 * @return true if the last step caused the snake to eat the food, thus signaling to the caller that they should
	 * call {@link #spawnFood(int, int)} to place another food.
	 */
	public boolean foodEaten() {
		return food == null;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Point getHead() {
		return snakeBody.isEmpty() ? null : new Point(snakeBody.getFirst());
	}

	public Point getTail() {
		return snakeBody.isEmpty() ? null : new Point(snakeBody.getLast());
	}

	public Point getFood() {
		return food == null ? null : new Point(food);
	}

	public Deque<Point> getBody() { return snakeBody; }

	public int getStep() {
		return step;
	}

	public int getSnakeSize() {
		return snakeSize;
	}

	/**
	 * Paint the board to the given applet. Uses the applet's stroke and fill settings, drawing squares for each cell.
	 *
	 * @param applet where to paint
	 */
	public void paint(PApplet applet) {
		applet.pushStyle();
		//Food
		applet.fill(COLOR_FOOD);
		applet.noStroke();
		if (food != null)
			applet.ellipse(food.x * SCALE + SCALE / 2, food.y * SCALE + SCALE / 2, SCALE, SCALE);

		//Snake body
		applet.fill(COLOR_SNAKE);
		Point tail = getTail();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (snakeSegmentAt(x, y) && !(tail.x == x && tail.y == y))
					applet.rect(x * SCALE, y * SCALE, SCALE, SCALE);
			}
		}

		//Snake head
		applet.fill(COLOR_OUTLINE);
		Point head = getHead();
		applet.rect(head.x * SCALE, head.y * SCALE, SCALE, SCALE);

		//Snake outline
		applet.stroke(COLOR_OUTLINE);
		for (int y = 1; y < height; y++) {
			for (int x = 1; x < width; x++) {
				//Draw lines in places where the snake butts up against itself, but not in places where the
				//snake segments are connected (differ by 1 in value)
				if (snakeSegmentAt(x, y) && snakeSegmentAt(x - 1, y)
						&& Math.abs(cells[y * width + x] - cells[y * width + (x - 1)]) != 1)
					applet.line(x * SCALE, y * SCALE, x * SCALE, (y + 1) * SCALE);
				if (snakeSegmentAt(x, y) && snakeSegmentAt(x, y - 1)
						&& Math.abs(cells[y * width + x] - cells[(y - 1) * width + x]) != 1)
					applet.line(x * SCALE, y * SCALE, (x + 1) * SCALE, y * SCALE);
			}
		}

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				//Outline the snake in places where you go from snake segment to no snake segment
				if (snakeSegmentAt(x, y) != (inBounds(x - 1, y) && snakeSegmentAt(x - 1, y)))
					applet.line(x * SCALE, y * SCALE, x * SCALE, (y + 1) * SCALE);
				if (snakeSegmentAt(x, y) != (inBounds(x, y - 1) && snakeSegmentAt(x, y - 1)))
					applet.line(x * SCALE, y * SCALE, (x + 1) * SCALE, y * SCALE);
			}
		}

		applet.popStyle();
	}
}
