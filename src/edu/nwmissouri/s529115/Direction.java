package edu.nwmissouri.s529115;

import java.awt.Point;

public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT;

	public static final Direction[] ALL = {UP, DOWN, LEFT, RIGHT};

	public Point applyTo(Point p) {
		if (this == UP)
			p.y--;
		if (this == DOWN)
			p.y++;
		if (this == LEFT)
			p.x--;
		if (this == RIGHT)
			p.x++;
		return p;
	}

	public static Direction fromTo(Point from, Point to) {
		int dx = to.x - from.x;
		int dy = to.y - from.y;
		if (dx == 0 && dy == -1)
			return UP;
		else if (dx == 0 && dy == 1)
			return DOWN;
		else if (dx == -1 && dy == 0)
			return LEFT;
		else if (dx == 1 && dy == 0)
			return RIGHT;
		else
			throw new IllegalArgumentException(String.format("%s and %s are not adjacent", from, to));
	}
}
