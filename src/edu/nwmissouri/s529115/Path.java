package edu.nwmissouri.s529115;

import processing.core.PApplet;

import java.awt.Point;
import java.util.ArrayList;

import static edu.nwmissouri.s529115.SnakeWindow.Cfg.*;

public class Path extends ArrayList<Direction> {
	public Point origin;

	public Path(Point origin) {
		this.origin = origin;
	}

	//Both of these are used as static local variables
	Point current = new Point();
	Point next = new Point();

	public Path(Path other) {
		super(other);
		this.origin = other.origin;
	}

	public void paint(PApplet applet, int color) {
		applet.stroke(color);
		next.setLocation(origin);
		for (int i = size() - 1; i >= 0; i--) {
			current.setLocation(next);
			get(i).applyTo(next);
			applet.line(current.x * SCALE + SCALE / 2, current.y * SCALE + SCALE / 2,
					next.x * SCALE + SCALE / 2, next.y * SCALE + SCALE / 2);
		}
	}

	public Direction step() {
		Direction dir = remove(size() - 1);
		dir.applyTo(origin);
		return dir;
	}

	@Override
	public String toString() {
		char[] buffer = new char[size()];
		for (int i = 0; i < size(); i++)
			buffer[i] = get(size() - 1 - i).toString().charAt(0);
		return new String(buffer);
	}
}
