package edu.nwmissouri.s529115;

import processing.core.PApplet;


import java.awt.event.KeyEvent;

import static edu.nwmissouri.s529115.SnakeWindow.Cfg.*;

public class SnakeWindow extends PApplet {
	/** Short for Configuration */
	public static class Cfg {
		public static final int
				WIDTH = 50,
				HEIGHT = 50,
				SCALE = 10,
				FRAMERATE = 9001,

				COLOR_SNAKE      = 0xFFFFFFFF,
				COLOR_FOOD       = 0xFF00FFFF,
				COLOR_OUTLINE    = 0xFFFF0000,
				COLOR_BACKGROUND = 0xFF333333,
				COLOR_PATH       = 0xFF009900,

				SNAKE_INIT_X = 5,
				SNAKE_INIT_Y = 5,
				SNAKE_INIT_SIZE = 4,

				STEP_SKIP = 0;
		public static final boolean
				SINGLE_STEP = false;
	}


	private final Board board = new Board(WIDTH, HEIGHT);
	private final Pathfinder pathfinder = new Pathfinder(board);
	private boolean shouldGrowSnake, drawOnce;

	@Override
	public void settings() {
		size(WIDTH * SCALE, HEIGHT * SCALE);
	}

	@Override
	public void setup() {
		frameRate(FRAMERATE);
		randomSeed(0);
		board.spawnSnake(SNAKE_INIT_X, SNAKE_INIT_Y, SNAKE_INIT_SIZE);
		spawnFoodRandomly();
	}

	@Override
	public void draw() {
		do {
			if (pathfinder.hasSteps()) {
				Direction dir = pathfinder.step();
				if (shouldGrowSnake) {
					board.growStep(dir);
					shouldGrowSnake = false;
				} else
					board.step(dir);

				if (board.foodEaten()) {
					shouldGrowSnake = true;
					spawnFoodRandomly();
				}
			} else {
				pathfinder.findPath();
				if (SINGLE_STEP)
					noLoop();
			}
		} while (board.getStep() < STEP_SKIP);

		background(COLOR_BACKGROUND);
		noStroke();
		board.paint(this);
		pathfinder.paint(this);
		if (drawOnce) {
			drawOnce = false;
			noLoop();
		}
	}

	@Override
	public void keyPressed() {
		if (SINGLE_STEP) {
			if (keyCode == KeyEvent.VK_SPACE) {
				loop();
			} else if (keyCode == RIGHT) {
				loop();
				drawOnce = true;
			}
		}
	}

	private void spawnFoodRandomly() {
		while(!board.spawnFood((int)random(WIDTH), (int)random(HEIGHT))) {
			//Retry until we find a valid location
		}
	}

	public static void main(String[] args) {
		PApplet.main(SnakeWindow.class, args);
	}
}
